terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "us-west-2"
}

resource "aws_instance" "groupe_5" {
  ami                    = "ami-0742afdcd4127131e"
  instance_type          = "t2.medium"
  vpc_security_group_ids = ["sg-a0525fe5"]
  key_name               = "exam_tp"
  tags                   = { Name = "groupe 5" }
}